/******************************************************************************
 * Copyright (c) 2002 - 2006 IBM Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *****************************************************************************/
package com.ibm.wala.eclipse.util;

import java.io.File;
import java.io.IOException;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;

import com.ibm.wala.cast.ir.ssa.AstIRFactory;
import com.ibm.wala.cast.java.ipa.callgraph.JavaSourceAnalysisScope;
import com.ibm.wala.cast.java.translator.polyglot.IRTranslatorExtension;
import com.ibm.wala.cast.java.translator.polyglot.JavaIRTranslatorExtension;
import com.ibm.wala.cast.java.translator.polyglot.PolyglotClassLoaderFactory;
import com.ibm.wala.classLoader.BinaryDirectoryTreeModule;
import com.ibm.wala.classLoader.ClassLoaderFactory;
import com.ibm.wala.classLoader.Module;
import com.ibm.wala.eclipse.util.EclipseProjectPath.Loader;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.SetOfClasses;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.SymbolTable;
import com.ibm.wala.ssa.Value;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.util.config.FileOfClasses;
import com.ibm.wala.util.debug.Assertions;

/**
 * 
 * @author julian dolby?
 * @author sfink .. cleaned up and refactored some 
 * @author smarkstr (added file extension support)
 *
 */
public class EclipseProjectSourceAnalysisEngine extends EclipseProjectAnalysisEngine {

  /**
   * file extension for source files in this Eclipse project
   */
  String fileExt = "java";
  
  public EclipseProjectSourceAnalysisEngine(IJavaProject project)
    throws JavaModelException, IOException 
  {
    super(project);
    //setCallGraphBuilderFactory(new ZeroCFABuilderFactory());
  }

  public EclipseProjectSourceAnalysisEngine(IJavaProject project, String fileExt)
    throws JavaModelException, IOException 
  {
    super(project);
    this.fileExt = fileExt;
  }

  @Override
  public AnalysisCache makeDefaultCache() {
    return new AnalysisCache(AstIRFactory.makeDefaultFactory(true));
  }

  protected AnalysisScope makeSourceAnalysisScope() {
    return new JavaSourceAnalysisScope();
  }

  @Override
  protected void buildAnalysisScope() {
    try {
      scope = makeSourceAnalysisScope(); 
      if (getExclusionsFile() != null) {
        scope.setExclusions(new FileOfClasses(new File(getExclusionsFile())));
      }
      EclipseProjectPath epath = getEclipseProjectPath();
      epath.resolveProjectClasspathEntries(fileExt);

      for (Module m : epath.getModules(Loader.PRIMORDIAL, true)) {
        scope.addToScope(scope.getPrimordialLoader(), m);
      }
      ClassLoaderReference app = scope.getApplicationLoader();
      for (Module m : epath.getModules(Loader.APPLICATION, true)) {
        scope.addToScope(app, m);
      }
      for (Module m : epath.getModules(Loader.EXTENSION, true)) {
        if (!(m instanceof BinaryDirectoryTreeModule))
          scope.addToScope(app, m);
      }
      ClassLoaderReference src = ((JavaSourceAnalysisScope)scope).getSourceLoader();
      for (Module m : epath.getModules(Loader.SOURCE, false)) {
        scope.addToScope(src, m);
      }

      
    } catch (JavaModelException e) {
      Assertions.UNREACHABLE();
    } catch (IOException e) {
      Assertions.UNREACHABLE();
    }
  }

  public IRTranslatorExtension getTranslatorExtension() {
    return new JavaIRTranslatorExtension();
  }

  protected ClassLoaderFactory getClassLoaderFactory(SetOfClasses exclusions,IRTranslatorExtension extInfo) {
    return new PolyglotClassLoaderFactory(exclusions, extInfo);
  }

  @Override
  public IClassHierarchy buildClassHierarchy() {
    IClassHierarchy cha = null;
    ClassLoaderFactory factory = getClassLoaderFactory(scope.getExclusions(), getTranslatorExtension());

    try {
      cha = ClassHierarchy.make(getScope(), factory);
    } catch (ClassHierarchyException e) {
      System.err.println("Class Hierarchy construction failed");
      System.err.println(e.toString());
      e.printStackTrace();
    }
    return cha;
  }

  @Override
  protected Iterable<Entrypoint> makeDefaultEntrypoints(AnalysisScope scope, IClassHierarchy cha) {
    return Util.makeMainEntrypoints(EclipseProjectPath.SOURCE_REF, cha);
  }

  @Override
  public AnalysisOptions getDefaultOptions(Iterable<Entrypoint> entrypoints) {
    AnalysisOptions options = new AnalysisOptions(getScope(), /* AstIRFactory.makeDefaultFactory(true), */entrypoints);

    SSAOptions ssaOptions = new SSAOptions();
    ssaOptions.setDefaultValues(new SSAOptions.DefaultValues() {
      public int getDefaultValue(SymbolTable symtab, int valueNumber) {
        Value v = symtab.getValue(valueNumber);
        if (v == null) {
          Assertions._assert(v != null, "no default for " + valueNumber);
        }
        return v.getDefaultValue(symtab);
      }
    });

    options.setSSAOptions(ssaOptions);

    return options;
  }
}
