/*******************************************************************************
 * Copyright (c) 2002 - 2006 IBM Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.ibm.wala.eclipse.cg.model;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;

import com.ibm.wala.cast.js.client.JavaScriptAnalysisEngine;
import com.ibm.wala.cast.js.translator.JavaScriptTranslatorFactory;
import com.ibm.wala.cast.js.util.WebUtil;
import com.ibm.wala.classLoader.SourceFileModule;
import com.ibm.wala.client.AbstractAnalysisEngine;
import com.ibm.wala.eclipse.util.CancelException;
import com.ibm.wala.eclipse.util.EclipseProjectSourceAnalysisEngine;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.util.warnings.WalaException;

abstract public class WalaProjectCGModel implements WalaCGModel {

  protected AbstractAnalysisEngine engine;

  protected CallGraph callGraph;

  protected Collection roots;

  protected WalaProjectCGModel(IJavaProject project) 
      throws JavaModelException, IOException 
  {
    this.engine = new EclipseProjectSourceAnalysisEngine(project) {
      @Override
      protected Iterable<Entrypoint> makeDefaultEntrypoints(AnalysisScope scope, IClassHierarchy cha) {
        return getEntrypoints(scope, cha);
      }
    };
  }

  protected WalaProjectCGModel(String htmlScriptFile) {
    this.engine = new JavaScriptAnalysisEngine() {

      {
        setTranslatorFactory(new JavaScriptTranslatorFactory.CAstRhinoFactory());
      }

      @Override
      protected Iterable<Entrypoint> makeDefaultEntrypoints(AnalysisScope scope, IClassHierarchy cha) {
        return getEntrypoints(scope, cha);
      }
    };

    SourceFileModule script = WebUtil.extractScriptFromHTML(htmlScriptFile);
    engine.setModuleFiles(Collections.singleton(script));
  }

  public void buildGraph() throws WalaException, CancelException {
    try {
      callGraph = engine.buildDefaultCallGraph();
      roots = inferRoots(callGraph);
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public CallGraph getGraph() {
    return callGraph;
  }

  public Collection getRoots() {
    return roots;
  }

  abstract protected Iterable<Entrypoint> getEntrypoints(AnalysisScope scope, IClassHierarchy cha);

  abstract protected Collection inferRoots(CallGraph cg) throws WalaException;

}
