/*******************************************************************************
 * Copyright (c) 2002 - 2006 IBM Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package com.ibm.wala.eclipse.tests;

import java.util.Collection;

import junit.framework.TestCase;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import com.ibm.wala.eclipse.cg.model.WalaCGModel;
import com.ibm.wala.eclipse.cg.model.WalaJarFileCGModelWithMain;
import com.ibm.wala.eclipse.util.CancelException;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.util.warnings.WalaException;

/**
 * Smoke test on the Wala call graph Eclipse view
 * 
 * @author aying
 */
public class WalaCGModelTest extends TestCase {

  @Override
  public void setUp() {
    System.out.println("Importing projects into the test workspace");
    EclipseTestUtil.importZippedProject("HelloWorld.zip");
  }

  public void testSmokeTest() throws WalaException, CancelException, JavaModelException {
    // get the input
    String appJarFullPath = getHelloWorldJar().getRawLocation().toString();

    // compute the call graph
    WalaCGModel model = new WalaJarFileCGModelWithMain(appJarFullPath);
    model.buildGraph();

    Collection roots = model.getRoots();
    assertNotNull(roots);

    Graph graph = model.getGraph();
    // The following is a rather weak condition, but it's better than nothing...
    assertGreaterOrEqual(graph.getNumberOfNodes(), 7);
  }

  private void assertGreaterOrEqual(int i, int min) {
    if (i < min) {
      fail("expected at least " + min + " but was " + i);
    }
  }

  public static IJavaProject getHelloWorldProject() {
    IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
    IJavaModel javaModel = JavaCore.create(workspaceRoot);
    IJavaProject helloWorldProject = javaModel.getJavaProject("HelloWorld");
    return helloWorldProject;
  }

  public static IFile getHelloWorldJar() {
    IJavaProject project = getHelloWorldProject();
    IFile helloWorldJar = project.getProject().getFile("helloWorld.jar");
    return helloWorldJar;
  }

}
